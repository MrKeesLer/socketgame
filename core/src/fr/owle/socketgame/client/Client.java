package fr.owle.socketgame.client;

import fr.owle.socketgame.game.GameObject;

import java.net.Socket;
import java.util.List;

public class Client {

    private Socket socket;
    private List<Client> otherClient;
    private GameObject player;

    public Client(Socket socket,List<Client> other){
        this.socket = socket;
        this.otherClient = other;
        this.player = new GameObject();
    }

    public void updateClientList(Client c){
        this.otherClient.add(c);
    }

    public Socket getSocket() {
        return socket;
    }
}
